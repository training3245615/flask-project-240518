FROM python:3.8.19-slim-bullseye

WORKDIR app

EXPOSE 8000

COPY requirements-server.txt requirements.txt .

RUN pip install -r requirements.txt && pip install -r requirements-server.txt

COPY . .

CMD gunicorn app:app -b 0.0.0.0:8000
